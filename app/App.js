// React
import React, { Component } from 'react';

// Redux
import { compose, createStore, applyMiddleware } from 'redux';
import reducer from './reducer';

import { devTools, persistState } from 'redux-devtools';
import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react';

// React DnD
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import Style from './style';

// Components
import WidgetToolbarItem from './components/WidgetToolbarItem/WidgetToolbarItem';
import SectionToolbarItem from './components/Section/SectionToolbarItem';
import Canvas from './components/Canvas/Canvas';

/**
 * Store
 * @param  {[type]} reducer [description]
 * @return {[type]}         [description]
 */
export const store = createStore(reducer);

store.dispatch({
  type: 'SET_STATE',
  state: {
    vote: {
      pair: ['Sunshine', '28 Days Later'],
      tally: {Sunshine: 2}
    }
  }
});

console.log(store.getState());

/**
 * App
 *
 * @param
 * @returns
 */
class App extends Component {

  render() {
    return (
      <section id="studioX">

        <div id="leftSidebar" style={Style.leftSidebar}>

          <i className="fa fa-trademark" style={{color: "white", margin: "0 9px", padding: "11px 0"}}></i>

          <SectionToolbarItem />
          <WidgetToolbarItem type="TEXT" title="Text">
            <i className="fa fa-file-text-o"></i>
          </WidgetToolbarItem>
          <WidgetToolbarItem type="IMAGE" title="Image">
            <i className="fa fa-picture-o"></i>
          </WidgetToolbarItem>
          <WidgetToolbarItem type="VIDEO" title="Video">
            <i className="fa fa-video-camera"></i>
          </WidgetToolbarItem>
          <WidgetToolbarItem type="FORM" title="Form">
            <i className="fa fa-align-justify"></i>
          </WidgetToolbarItem>
          <WidgetToolbarItem type="AUDIO" title="Audio">
            <i className="fa fa-music"></i>
          </WidgetToolbarItem>
          <WidgetToolbarItem type="CHANNELFINDER" title="Channel Finder">
            <i className="fa fa-television"></i>
          </WidgetToolbarItem>
          <WidgetToolbarItem type="LINKS" title="Link">
            <i className="fa fa-list"></i>
          </WidgetToolbarItem>
        </div>

        <div id="rightSidebar" style={Style.rightSidebar}>

        </div>

        <div id="titleBar" style={Style.titleBar}>

        </div>

        <div id="viewSelector" style={Style.viewSelector}>

        </div>

        <Canvas
          style={Style.canvas}
          theme={Style.theme} />

      </section>
    );
  }
}

export default DragDropContext(HTML5Backend)(App);
