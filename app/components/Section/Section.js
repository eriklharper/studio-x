'use strict';

import React from 'react';

/**
 * Section
 * Renders a Section element to the Canvas.
 *
 * @prop
 */
var Section = React.createClass({

  /**
   * Render
   * @return {[type]} [description]
   */
  render: function() {
    return (
      <section style={this.props.style}>
        {this.props.children}
      </section>
    );
  }

});

module.exports = Section;
