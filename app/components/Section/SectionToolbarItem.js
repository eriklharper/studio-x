'use strict';

// React
import React from 'react';
import { DragSource } from 'react-dnd';
import { ItemTypes } from '../../ItemTypes';

/**
 * React DnD DragSource
 *
 * @param
 * @returns
 */
const source = {
  beginDrag(props) {
    return {
      type: props.type
    }
  }
};

/**
 * React DnD Collect Function
 *
 * @param
 * @returns
 */
function collect(connect, monitor) {
  return {
    connectDragPreview: connect.dragPreview(),
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

/**
 * Section Toolbar Item
 *
 *
 * @prop
 */
var SectionToolbarItem = React.createClass({

  /**
   * Prop Types
   */
  propTypes: {
    connectDragSource: React.PropTypes.func.isRequired,
    isDragging: React.PropTypes.bool.isRequired,
    style: React.PropTypes.object,
  },

  /**
   * Render
   * @return {[type]} [description]
   */
  render: function() {

    var connectDragSource = this.props.connectDragSource;

    var itemStyle = {
      color: "white",
      margin: "0 9px",
      padding: '11px 0'
    }

    var wrapperStyles = {
      opacity: this.props.isDragging ? 0.5 : 1,
      cursor: 'move'
    }

    return connectDragSource(
      <div className='SectionToolbarItem' style={itemStyle} title={this.props.title}>
        <div style={wrapperStyles}>
          <i className="fa fa-square-o"></i>
        </div>
      </div>
    );
  }

});

module.exports = DragSource(ItemTypes.SECTION, source, collect)(SectionToolbarItem);
