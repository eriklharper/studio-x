'use strict';

// React
var React = require('react');

/**
 * Widget
 *
 *
 * @prop
 */
var Widget = React.createClass({

  /**
   * Prop Types
   */
  propTypes: {
    style: React.PropTypes.object,
    type: React.PropTypes.string
  },

  /**
   * Render
   * @return {[type]} [description]
   */
  render: function() {
    var style = {
      backgroundColor: "#333",
      padding: "10px",
      color: "#FFF"
    };

    return (
      <div className='widget' style={style}>
        {this.props.title}
      </div>
    );
  }

});

module.exports = Widget;
