jest.autoMockOff();

//React
var React = require('react/addons');
var TestUtils = React.addons.TestUtils;

//Components
var Widget = require('../widget');

//Modules
var Igniter = require('../../../modules/igniter');
var testHelper = require('../../../../test/test-helper');
var $ = require('jquery');
var is = require('is_js');
var IgniterData = require('../../../../test/igniter-fixtures.js').igniterData;

describe('<Widget>', function() {
  it('Renders properly with no children or properties', function() {

    // Render Link group without props
    var component = TestUtils.renderIntoDocument(
      <Widget></Widget>
    );

    // Verify that it's Off by default
    var domElement = TestUtils.findRenderedDOMComponentWithTag(component, 'div');

    expect(domElement.getDOMNode().tagName).toEqual('DIV');
    expect(domElement.getDOMNode().className).toEqual('widget');

  });

  it('Renders properly with a style prop', function() {

    // Render Link group without props
    var component = TestUtils.renderIntoDocument(
      <Widget style={{backgroundColor: 'blue'}}></Widget>
    );

    // Verify that it's Off by default
    var domElement = TestUtils.findRenderedDOMComponentWithTag(component, 'div');

    expect(domElement.getDOMNode().style['background-color']).toEqual('blue');

  });
});
