'use strict';

// React
import React, { PropTypes } from 'react';
import { DragSource } from 'react-dnd';
import { ItemTypes } from '../../ItemTypes';

// Components
var Widget = require('../Widget/Widget');

/**
 * React DnD DragSource
 *
 * @param
 * @returns
 */
const source = {
  beginDrag(props) {
    const { id, title, left, top, type } = props;
    return { id, title, left, top, type };
  }
};

/**
 * React DnD Collect Function
 *
 * @param
 * @returns
 */
function collect(connect, monitor) {
  return {
    connectDragPreview: connect.dragPreview(),
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

/**
 * Get Styles
 *
 * @param
 * @returns
 */
function getStyles(props) {
  const { left, top, isDragging } = props;
  const transform = `translate3d(${left}px, ${top}px, 0)`;

  return {
    position: 'absolute',
    transform: transform,
    WebkitTransform: transform,
    // IE fallback: hide the real node using CSS when dragging
    // because IE will ignore our custom "empty image" drag preview.
    opacity: isDragging ? 0 : 1,
    height: isDragging ? 0 : ''
  };
}

/**
 * DraggableWidget
 *
 *
 * @prop
 */
var DraggableWidget = React.createClass({

  /**
   * Prop Types
   */
  propTypes: {
    style: React.PropTypes.object,
    connectDragSource: PropTypes.func.isRequired,
    connectDragPreview: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    id: PropTypes.any.isRequired,
    title: PropTypes.string.isRequired,
    left: PropTypes.number.isRequired,
    top: PropTypes.number.isRequired,
    type: React.PropTypes.string
  },

  /**
   * Render
   * @return {[type]} [description]
   */
  render: function() {
    const { title, type, connectDragSource } = this.props;
    return connectDragSource(
      <div className='draggablewidget' style={getStyles(this.props)}>
        <Widget type={type} title={title} />
      </div>
    );
  }

});

module.exports = DragSource(ItemTypes.WIDGET, source, collect)(DraggableWidget);
