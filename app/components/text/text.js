'use strict';

// React
var React = require('react');

/**
 * Text
 * Renders a single block of text.
 *
 * @prop string htmlTag - Html Tag to render with.
 * @prop object style - Object containing style information to inject directly onto the component.
 */
var Text = React.createClass({

  propTypes: {
    htmlTag: React.PropTypes.oneOf([
      'h1',
      'h2',
      'h3',
      'h4',
      'h5',
      'h6',
      'p',
      'span',
      'div']),
    style: React.PropTypes.object
  },

  render: function() {

    var Tag = this.props.htmlTag || 'div';

    return (
      <Tag className='text' style={this.props.style}>
        {this.props.children}
      </Tag>
    );

  }

});

module.exports = Text;
