jest.dontMock('../text.js');

var React = require('react/addons');
var TestUtils = React.addons.TestUtils;
var Text = require('../text.js');

// Setup
console.warn = jest.genMockFunction();

/**
 * <Text> Component Tests
 *
 */
describe('<Text>', function() {

  var tags = [
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'p',
    'span',
    'div'
  ];

  tags.forEach(function(tag, i){
    it('Should render an '+tag+' element', function() {

      // Render into Document
      var TextRender = TestUtils.renderIntoDocument(
        <Text htmlTag={tag}>
          content text {i}
        </Text>

      );

      // Find Rendered DOM Component
      var TextElm = TestUtils.findRenderedDOMComponentWithTag(TextRender, tag);

      // Assert Results
      expect(TextElm.getDOMNode().textContent).toEqual('content text ' + i);
    });

  });

  it("Should display a div when the htmlTag prop is empty.", function() {

    var component = TestUtils.renderIntoDocument(
      <Text />
    );

    var element = React.findDOMNode(component);

    expect(element.tagName).toEqual("DIV");

  });

  it("Should warn on the console when a disallowed htmlTag prop is supplied.", function() {

    var component = TestUtils.renderIntoDocument(
      <Text htmlTag="bogusTag" />
    );

    expect(console.warn.mock.calls.length).toBeGreaterThan(0);

  });

});
