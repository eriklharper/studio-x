'use strict';

// React
import React from 'react';
import update from 'react/lib/update';
import { DropTarget } from 'react-dnd';

// Components
import DraggableWidget from '../DraggableWidget/DraggableWidget';
import Text from '../text/text';

// Modules
import { ItemTypes } from '../../ItemTypes';

/**
 * Collect
 *
 * @param
 * @returns
 */
function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

/**
 * React DnD Drop Target
 *
 * @param
 * @returns
 */
var target = {
  drop(props, monitor, component) {
    console.log("Dropped " + monitor.getItem().type);

    const delta = monitor.getDifferenceFromInitialOffset();
    const item = monitor.getItem();
    const type = item.type;

    switch (type) {
      case "SECTION":
        component.addWidget('section');
        break;
      case "TEXT":
        component.addWidget('text');
        break;
      case "IMAGE":
        component.addWidget('image');
        break;
      case "VIDEO":
        break;
      case "FORM":
        break;
      case "AUDIO":
        break;
      case "CHANNELFINDER":
        break;
      case "LINKS":
        break;
      default:

        let left = Math.round(item.left + delta.x);
        let top = Math.round(item.top + delta.y);

        component.addWidget('section');
        // component.moveWidget(item.id, left, top);
        break;
    }
    // if (props.snapToGrid) {
    //   [left, top] = snapToGrid(left, top);
    // }

  },
  hover(props, monitor, component) {
    console.log("Hovering with " + monitor.getItem().type);
  }
};

/**
 * Canvas
 *
 *
 * @prop
 */
var Canvas = React.createClass({

  /**
   * Prop Types
   */
  propTypes: {
    theme: React.PropTypes.object
  },

  /**
   * Get Initial State
   * @return {[type]} [description]
   */
  getInitialState: function () {
    return {
      widgets: [
        { id: 'a', type: 'SECTION' },
        { id: 'b', type: 'SECTION' }
      ]
    }
  },

  addWidget: function (type) {
    this.setState(update(this.state.widgets, {
      $push: [{
        title: type,
        type: type
      }]
    }));
  },

  /**
   * Move Widget
   * @param  {[type]} id   [description]
   * @param  {[type]} left [description]
   * @param  {[type]} top  [description]
   * @return {[type]}      [description]
   */
  moveWidget: function(id, left, top) {
    var key,
        widgetToUpdate;

    this.state.widgets.forEach((widget, i) => {
      if (widget.id == id) {
        key = i;
        widgetToUpdate = widget;
      }
    });

    console.log(this.state.widgets[key]);

    var newState = update(this.state.widgets[key], {$merge: {
      left: left,
      top: top
    }});

    console.log(newState);

    this.setState(newState);
  },

  /**
   * Render Widget
   * @param  {[type]} item [description]
   * @param  {[type]} key  [description]
   * @return {[type]}      [description]
   */
  renderWidget: function (item, key) {
    var widget;

    switch (item.type) {
      case 'SECTION':

        var Section = require('../Section/Section'),
            style = {
              width: '100%',
              height: '50px',
              textAlign: 'center'
            };

        widget = <Section style={style}>New Section</Section>;
        break;
    }

    return widget;
    // return (
    //   <DraggableWidget
    //     key={key}
    //     id={key}
    //     {...item} />
    // );
  },

  /**
   * Render
   * @return {[type]} [description]
   */
  render: function() {

    var connectDropTarget = this.props.connectDropTarget;

    const { widgets } = this.state;
    const content = [];
    console.log(widgets);

    widgets.forEach((widget, i) => {
      content.push(this.renderWidget(widget, widget.id));
    });

    return connectDropTarget(
      <div className='canvas' style={this.props.style}>
        {content}
      </div>
    );
  }

});

var ItemTypesArray = [];
for (var key in ItemTypes) {
  ItemTypesArray.push(ItemTypes[key]);
}

module.exports = DropTarget(ItemTypesArray, target, collect)(Canvas);
