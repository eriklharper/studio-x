/**
 * Style
 * @type {Object}
 */
var theme = {
  panel: {
    background: "#4b4b4b",
    border: "1px solid #252525"
  }
}

var style = {
  canvas: {
    height: "100%"
  },
  propertiesPanel: {
    float: "right",
    width: "250px",
    height: "100%",
    backgroundColor: theme.panel.background,
    border: theme.panel.border
  },
  titleBar: {
    backgroundColor: theme.panel.background,
    border: theme.panel.border
  },
  viewSelector: {

  },
  leftSidebar: {
    backgroundColor: theme.panel.background,
    height: "100%",
    left: 0,
    position: "fixed",
    top: 0,
    width: "35px"
  },
  rightSidebar: {
    position: "fixed",
    width: "241px",
    height: "100%",
    right: 0,
    top: 0,
    backgroundColor: "#3d3d3d",
    borderLeft: theme.panel.border,
    padding: 0,
    zIndex: 14
  },
  theme: theme
}

module.exports = style;
