import {List, Map} from 'immutable';

/**
 * Set State
 * @param {[type]} state    [description]
 * @param {[type]} newState [description]
 */
function setState(state, newState) {
  return state.merge(newState);
}

/**
 * Root Reducer
 * @param  {[type]} state =             Map( [description]
 * @return {[type]}       [description]
 */
export default function(state = Map(), action) {
  switch (action.type) {
    case 'DROPPED_SECTION':
    case 'DROPPED_CONTAINER':
    case 'SET_STATE':
      return setState(state, action.state);
  }
  return state;
}
