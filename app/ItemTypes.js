export const ItemTypes = {
  BUTTON: 'button',
  COLUMNS: 'columns',
  CONTAINER: 'container',
  DIVBLOCK: 'divblock',
  LINKBLOCK: 'linkblock',
  SECTION: 'section',
  TEXT: 'text',
  WIDGET: 'widget'
};
